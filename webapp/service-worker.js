const cacheName = 'performOfflineActivity-v0.1';
const resourcesToPrecache = [
     './',
     './index.html'
];

self.addEventListener('install', event => {
     event.waitUntil(
          caches.open(cacheName).then(function(cache) {
               return cache.addAll(resourcesToPrecache)
          }).then(function() {
               console.log('precache success');
          }).catch(function(err) {
               console.log(err);
          })
     );
});

self.addEventListener('activate', event => {
     console.log('service worker activated');
     self.clients.claim()
     const cacheAllowlist = [cacheName];
     event.waitUntil(
          caches.keys().then(function(cacheNames) {
               return Promise.all(
                    cacheNames.map(function(name) {
                         if (cacheAllowlist.indexOf(name) === -1) {
                              return caches.delete(name);
                         }
                    })
               );
          })
     );
});

self.addEventListener('fetch', function (event) {
     event.respondWith(
          caches.open(cacheName).then(function (cache) {
               return cache.match(event.request).then(function (response) {
                    return response || fetch(event.request).then(function (response) {
                         cache.put(event.request, response.clone());
                         return response;
                    });
               });
          })
     );
});