sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"bowdark/performOfflineActivity/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("bowdark.performOfflineActivity.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			
			this.getModel("json").setProperty("/online", navigator.onLine);
			window.addEventListener("offline", function() {
			    this.getModel("json").setProperty("/online", false);
			}.bind(this));
			window.addEventListener("online", function() {
			    this.getModel("json").setProperty("/online", true);
			}.bind(this));

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			
		}
	});
});