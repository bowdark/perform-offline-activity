sap.ui.define([
	"bowdark/performOfflineActivity/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("bowdark.performOfflineActivity.controller.App", {
		onInit: function () {
			var oSystem = this.getModel("device").getProperty("/system");
			if (oSystem.desktop) {
				this.getView().addStyleClass("sapUiSizeCompact");
			}
		}
	});
});