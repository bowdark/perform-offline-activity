sap.ui.define([
	"bowdark/performOfflineActivity/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/core/Fragment",
	"sap/m/MessageBox",
	"bowdark/performOfflineActivity/controller/OperationActivityDataExtractor"
], function (BaseController, Filter, Fragment, MessageBox, OAExtractor) {
	"use strict";
	/* eslint-disable no-console */ /* global bowdark */

	return BaseController.extend("bowdark.performOfflineActivity.controller.Execution", {
		
		_sShopFloorItemPath: "",
		
		onInit: function() {
			bowdark.performOfflineActivity.controller.BaseController.prototype.onInit.apply(this, arguments);
			this.getOwnerComponent().getRouter().getRoute("execution").attachPatternMatched(this._onPatternMatched, this);
		},
		
		onPressBackToWorklist: function() {
			this.getOwnerComponent().getRouter().navTo("worklist");
		},
		
		getCurrentSFI: function() {
			return Object.assign({}, this.getModel("json").getProperty(this._sShopFloorItemPath));
		},
		
		setSFIProperty: function(sProperty, vValue) {
			this.getModel("json").setProperty(this._sShopFloorItemPath + "/" + sProperty, vValue);
		},
		
		_onPatternMatched: function(oEvent) {
			var sPath = oEvent.getParameter("arguments").path;
			if (!sPath || typeof sPath !== "string") {
				this._showInvalidSFIMessage();
				return;
			}
			this._sShopFloorItemPath = sPath.replace(/\_/g, "/");
			this.loadRequiredDataFromIDB().then(function() {
				var oShopFloorItem = this.getCurrentSFI();
				if (!oShopFloorItem) {
					this._showInvalidActivityMessage();
				} else {
					this.getView().bindElement({ path: this._sShopFloorItemPath, model: "json" });
					this._filterInspTable();
					this._filterPRTsTable();
					this._filterComponentsTable();
					this._getDocsFromIDB();
					this._refreshAttachmentsFromIDB();
					this.getView().setBusy(false);
				}
			}.bind(this));
		},
		
		_showInvalidActivityMessage: function() {
			MessageBox.error("Could not find Activity", {
				onClose: function() {
					this.getOwnerComponent().getRouter().navTo("worklist");
				}.bind(this)
			});
		},
		
		_filterPRTsTable: function() {
			var oShopFloorItem = this.getCurrentSFI();
			var aFilters = [
				new Filter("OpActyNtwkElement", "EQ", oShopFloorItem.OpActyNtwkElement),
				new Filter("OpActyNtwkInstance", "EQ", oShopFloorItem.OpActyNtwkInstance),
				new Filter("ProdnRsceToolCategory", "NE", "D")
			];
			this.byId("prtsTable").getBinding("items").filter(aFilters);
		},
		
		_filterComponentsTable: function() {
			var oShopFloorItem = this.getCurrentSFI();
			var aFilters = [
				new Filter("OpActyNtwkElement", "EQ", oShopFloorItem.OpActyNtwkElement),
				new Filter("OpActyNtwkInstance", "EQ", oShopFloorItem.OpActyNtwkInstance)
				// new Filter("ShopFloorItem", "EQ", oShopFloorItem.ShopFloorItem)
			];
			this.byId("componentsTable").getBinding("items").filter(aFilters);
		},
		
		_getDocsFromIDB: function() {
			var oShopFloorItem = this.getCurrentSFI();
			var aDocs = [];
			Promise.all(
				this.getModel("json").getProperty("/prts").filter(function(oPRT) {
					return oPRT.ProdnRsceToolCategory === "D" 
						&& oPRT.OpActyNtwkElement === oShopFloorItem.OpActyNtwkElement
						&& oPRT.OpActyNtwkInstance === oShopFloorItem.OpActyNtwkInstance;
				}).map(function(oDocRecord) {
					return this._oIDBClient.get("documents", {
						Documenttype: oDocRecord.DocumentType,
						Documentnumber: oDocRecord.DocumentInfoRecord,
						Documentpart: oDocRecord.DocumentPart,
						Documentversion: oDocRecord.DocumentVersion
					}).then(function(oDoc) {
						aDocs.push(oDoc);
					});
				}.bind(this))
			).then(function() {
				this.getModel("json").setProperty("/documents", aDocs);
			}.bind(this));
		},
		
		_refreshAttachmentsFromIDB: function() {
			var oShopFloorItem = this.getCurrentSFI();
			this._oIDBClient.getAll("attachments").then(function(aResults) {
				var aAttachments = aResults.filter(function(oAttachment) {
					return oAttachment.OpActyNtwkElement === oShopFloorItem.OpActyNtwkElement
						&& oAttachment.OpActyNtwkInstance === oShopFloorItem.OpActyNtwkInstance
						&& oAttachment.ShopFloorItem === oShopFloorItem.ShopFloorItem;
				});
				this.getModel("json").setProperty("/attachments", aAttachments);
			}.bind(this));
		},
		
		_refreshAttachmentsFromServer: function() {
			var oExtractor = new OAExtractor(this.getOwnerComponent());
			var oShopFloorItem = this.getCurrentSFI();
			oExtractor.extractAttachmentsMetadata([oShopFloorItem]).then(function() {
				return oExtractor.extractAllFiles();
			}).then(function() {
				return Promise.all(
					oExtractor.aAttachments.map(function(oAttachment) {
						this._oIDBClient.put("attachments", oAttachment);
					}.bind(this))
				);
			}.bind(this)).then(function() {
				this._refreshAttachmentsFromIDB();
			}.bind(this));
		},
		
		_filterInspTable: function() {
			var oShopFloorItem = this.getCurrentSFI();
			var aFilters = [
				new Filter("OpActyNtwkElement", "EQ", oShopFloorItem.OpActyNtwkElement),
				new Filter("OpActyNtwkInstance", "EQ", oShopFloorItem.OpActyNtwkInstance),
				new Filter("ShopFloorItem", "EQ", oShopFloorItem.ShopFloorItem)
			];
			this.byId("inspTable").getBinding("items").filter(aFilters);
		},
		
		onPressOpenFile: function(oEvent) {
			var oDoc = oEvent.getSource().getBindingContext("json").getObject();
			var oBlob = new Blob([oDoc.arrayBuffer], { type: oDoc.ContentType });
			window.open(URL.createObjectURL(oBlob));
		},
		
		onPressDeleteOfflineAttachment: function(oEvent) {
			var oAttachment = oEvent.getSource().getBindingContext("json").getObject();
			this._oIDBClient.delete("attachments", oAttachment).then(function() {
				this._refreshAttachmentsFromIDB();
			}.bind(this));
		},
		
		onAttachmentsUploaderChange: function(oEvent) {
			if (!oEvent.getParameter("files")) {
				return;
			}
			var oShopFloorItem = this.getCurrentSFI();
			var oFileList = oEvent.getParameter("files");
			var oUploadedFile = oFileList.item(0);
			var sTempDocNumber = String((new Date()).getTime());
			var oNewAttachment = {
				Documentnumber: sTempDocNumber,
				Documentpart: "00",
				Documentversion: "00",
				Documenttype: "00",
				FileId: sTempDocNumber,
				Filename: oUploadedFile.name,
				Filesize: oUploadedFile.size,
				ContentType: oUploadedFile.type,
				CreatedAt: oUploadedFile.lastModifiedDate,
				createdOffline: true,
				OpActyNtwkElement: oShopFloorItem.OpActyNtwkElement,
				OpActyNtwkInstance: oShopFloorItem.OpActyNtwkInstance,
				ShopFloorItem: oShopFloorItem.ShopFloorItem
			};
			oUploadedFile.arrayBuffer().then(function(oArrayBuffer) {
				oNewAttachment.arrayBuffer = oArrayBuffer;
				return this._oIDBClient.put("attachments", oNewAttachment);
			}.bind(this)).then(function() {
				this.setSFIProperty("syncStatus", "Pending Changes");
				this._oIDBClient.put("C_ShopFloorItemAtOpActy", this.getCurrentSFI());
				this._refreshAttachmentsFromIDB();
				this.byId("attachmentsCollection").getBinding("items").refresh(true);
				this.byId("attachmentsUploader").clear();
			}.bind(this));
		},
		
		onComponentQuantityInputChange: function(oEvent) {
			var oContext = oEvent.getSource().getBindingContext("json");
			var sPath = oContext.getPath();
			var iValue = Number(oEvent.getSource().getValue());
			this.getModel("json").setProperty(sPath + "/MaterialComponentQuantity", iValue);
			var oComponent = Object.assign({}, oContext.getObject());
			this._oIDBClient.put("C_OperationActivityComponent", oComponent);
			this.setSFIProperty("syncStatus", "Pending Changes");
			this._oIDBClient.put("C_ShopFloorItemAtOpActy", this.getCurrentSFI());
		},
		
		formatQualInspection: function(sGroup, sCode) {
			if (sGroup && sCode) {
				return sGroup + " - " + sCode;
			} else {
				return sCode;
			}
		},
		
		onQuantInspValueChange: function(oEvent) {
			this.setSFIProperty("syncStatus", "Pending Changes");
			var oContext = oEvent.getSource().getBindingContext("json");
			var oInspection = Object.assign({}, oContext.getObject());
			if (oInspection.InspSpecIsQuantitative === "1") {
				var iValue = Number(oEvent.getSource().getValue());
				var bValid = iValue >= Number(oInspection.InspSpecLowerLimit) && iValue <= Number(oInspection.InspSpecUpperLimit);
				oInspection.InspectionValuationResult = bValid ? "A" : "R";
				oInspection.InspectionResultMeanValue = iValue.toExponential();
				this.getModel("json").setProperty(oContext.getPath(), oInspection);
				this._oIDBClient.put("C_SFIPlanInspCharInfo", oInspection);
				this._oIDBClient.put("C_ShopFloorItemAtOpActy", this.getCurrentSFI());
			}
		},
		
		onPressInspValueHelpRequest: function(oEvent) {
			var oInput = oEvent.getSource();
			var sPath = oInput.getBindingContext("json").getPath();
			var oInspection = Object.assign({}, oInput.getBindingContext("json").getObject());
			this.setSFIProperty("syncStatus", "Pending Changes");
			this._oIDBClient.put("C_ShopFloorItemAtOpActy", this.getCurrentSFI());
			var aVHFilters = [
				new Filter("InspectionLot", "EQ", oInspection.InspectionLot),
				new Filter("InspPlanOperationInternalID", "EQ", oInspection.InspPlanOperationInternalID),
				new Filter("InspectionCharacteristic", "EQ", oInspection.InspectionCharacteristic)
			];
			Fragment.load({
				id: this.getView().getId(),
				name: "bowdark.performOfflineActivity.fragment.InspVHDialog",
				controller: this
			}).then(function(oDialog) {
				this.getView().addDependent(oDialog);
				oDialog.open();
				oDialog.attachAfterClose(function() {
					oDialog.destroy();
				});
				oDialog.getEndButton().attachPress(function() {
					oDialog.close();
				});
				this.byId("inspVhList").getBinding("items").filter(aVHFilters);
				this.byId("inspVhList").getBinding("items").resume();
				this.byId("inspVhList").attachItemPress(function(oPressEvent) {
					var oSelectedCode = oPressEvent.getParameter("listItem").getBindingContext("json").getObject();
					oInspection.CharacteristicAttributeCode = oSelectedCode.CharacteristicAttributeCode;
					oInspection.CharacteristicAttributeCodeGrp = oSelectedCode.CharacteristicAttributeCodeGrp;
					oInspection.InspectionValuationResult = oSelectedCode.CharcAttributeValuation;
					// oInput.setValue(oSelectedCode.CharacteristicAttributeCodeGrp + " - " + oSelectedCode.CharacteristicAttributeCode);
					this.getModel("json").setProperty(sPath, oInspection);
					this._oIDBClient.put("C_SFIPlanInspCharInfo", oInspection);
					oDialog.close();
				}, this);
			}.bind(this));
		},
		
		onInspRemarksChange: function(oEvent) {
			var oInspection = Object.assign({}, oEvent.getSource().getBindingContext("json").getObject());
			this._oIDBClient.put("C_SFIPlanInspCharInfo", oInspection);
		},
		
		onPressStart: function() {
			var oShopFloorItem = this.getCurrentSFI();
			var oStartAction = {
				OpActyNtwkElement: oShopFloorItem.OpActyNtwkElement,
				OpActyNtwkInstance: oShopFloorItem.OpActyNtwkInstance,
				ShopFloorItem: oShopFloorItem.ShopFloorItem,
				Type: "START",
				Time: new Date()
			};
			this.setSFIProperty("UserIsLaboredOn", true);
			this.setSFIProperty("StatusAndActionSchemaStatus", "SAP_IN_PROCESS");
			this._oIDBClient.put("labor", oStartAction);
			this._oIDBClient.put("C_ShopFloorItemAtOpActy", this.getCurrentSFI());
		},
		
		onPressUndoStart: function() {
			var oShopFloorItem = this.getCurrentSFI();
			var oStartAction = {
				OpActyNtwkElement: oShopFloorItem.OpActyNtwkElement,
				OpActyNtwkInstance: oShopFloorItem.OpActyNtwkInstance,
				ShopFloorItem: oShopFloorItem.ShopFloorItem,
				Type: "START"
			};
			this.setSFIProperty("UserIsLaboredOn", false);
			this.setSFIProperty("StatusAndActionSchemaStatus", "SAP_INITIAL");
			this._oIDBClient.delete("labor", oStartAction);
			this._oIDBClient.put("C_ShopFloorItemAtOpActy", this.getCurrentSFI());
		},
		
		onPressComplete: function() {
			var oShopFloorItem = this.getCurrentSFI();
			var oCompleteAction = {
				OpActyNtwkElement: oShopFloorItem.OpActyNtwkElement,
				OpActyNtwkInstance: oShopFloorItem.OpActyNtwkInstance,
				ShopFloorItem: oShopFloorItem.ShopFloorItem,
				Type: "COMPLETE",
				Time: new Date()
			};
			this.setSFIProperty("syncStatus", "Pending Changes");
			this.setSFIProperty("UserIsLaboredOn", false);
			this.setSFIProperty("StatusAndActionSchemaStatus", "SAP_COMPLETED");
			this._oIDBClient.put("labor", oCompleteAction);
			this._oIDBClient.put("C_ShopFloorItemAtOpActy", this.getCurrentSFI());
		},
		
		onPressUndoComplete: function() {
			var oShopFloorItem = this.getCurrentSFI();
			var oCompleteAction = {
				OpActyNtwkElement: oShopFloorItem.OpActyNtwkElement,
				OpActyNtwkInstance: oShopFloorItem.OpActyNtwkInstance,
				ShopFloorItem: oShopFloorItem.ShopFloorItem,
				Type: "COMPLETE"
			};
			this.setSFIProperty("UserIsLaboredOn", true);
			this.setSFIProperty("StatusAndActionSchemaStatus", "SAP_IN_PROCESS");
			this._oIDBClient.delete("labor", oCompleteAction);
			this._oIDBClient.put("C_ShopFloorItemAtOpActy", this.getCurrentSFI());
		}
		
	});
});