sap.ui.define([
	"sap/ui/base/Object",
	"./IndexedDBClient",
	"./ODataModelClient",
	"sap/ui/model/Filter"
], function(SAPObject, IndexedDBClient, ODataModelClient, Filter) {
	/* eslint-disable no-loop-func */
	
	return SAPObject.extend("bowdark.performOfflineActivity.controller.OperationActivityDataExtractor", {
		
		_oIDB: {},
		_http: {},
		_sOrder: "",
		_aActivityKeyMaps: [],
		_loadingDialog: null,
		aActivities: [],
		aShopFloorItems: [],
		aComponents: [],
		aInspections: [],
		aInspectionValueHelps: [],
		aPRTs: [],
		aDocuments: [],
		aAttachments: [],
		
		constructor: function(oComponent, bUseBatch) {
			bUseBatch = typeof bUseBatch !== "boolean" ? bUseBatch : true;
			this._oIDB = new IndexedDBClient();
			if (!oComponent) {
				var aModels = this.createDefaultModels(bUseBatch);
				this._http = new ODataModelClient(aModels);
			} else {
				this._http = new ODataModelClient(oComponent);
			}
		},
		
		createDefaultModels: function(bUseBatch) {
			bUseBatch = typeof bUseBatch !== "boolean" ? bUseBatch : true;
			var aServices = [{
				name: "offline",
				service: "ZMPE_OFFLINE_SRV"
			}, {
				name: "inspections",
				service: "MPE_EXEC_RR_SRV"
			}, {
				name: "activities",
				service: "MPE_PERSONAL_WORK_QUEUE_SRV"
			}, {
				name: "execution",
				service: "MPE_SFI_EXECUTION_SRV"
			}, {
				name: "documents",
				service: "MPE_DOCUMENT_LIST_SRV"
			}, {
				name: "attachments",
				service: "CV_ATTACHMENT_SRV"
			}];
			return aServices.map(function(oService) {
				var oModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/" + oService.service, {
					defaultCountMode: "Inline",
					defaultBindingMode: "TwoWay",
					refreshAfterChange: false,
					preload: true,
					useBatch: bUseBatch
				});
				return {
					model: oModel,
					name: oService.name
				};
			});
		},
		
		setLoadingProgress: function(iPercentage) {
			try {
				this._loadingDialog.getContent()[0].getItems()[1].setPercentValue(iPercentage);
			} catch (oError) {
				//
			}
		},
		
		setLoadingState: function(sState) {
			try {
				this._loadingDialog.getContent()[0].getItems()[1].setState(sState);
			} catch (oError) {
				//
			}
		},
		
		_openLoadingDialog: function() {
			if (!this._loadingDialog) {
				this._loadingDialog = new sap.m.Dialog({
					showHeader: false,
					contentWidth: "500px",
					content: [
						new sap.m.VBox({
							justifyContent: "Center",
							items: [
								new sap.m.Text({text: "Exporting Data..."}),
								new sap.m.ProgressIndicator({
									width: "400px",
									state: "Information",
									percentValue: 0,
									showValue: false
								})
							]
						}).addStyleClass("sapUiMediumMargin")
					]
				});
			}
			this.setLoadingProgress(0);
			this.setLoadingState("Information");
			this._loadingDialog.open();
		},
		
		clearAllStores: function() {
			return this._oIDB.clearAllStores();
		},

		extract: function(vTarget, bTransferToIndexedDB, bShowLoadingDialog) {
			bShowLoadingDialog = bShowLoadingDialog !== undefined ? bShowLoadingDialog : true;
			bTransferToIndexedDB = bTransferToIndexedDB !== undefined ? bTransferToIndexedDB : true;
			if (bShowLoadingDialog) {
				this._openLoadingDialog();
			}
			if (Array.isArray(vTarget)) {
				this._aActivityKeyMaps = vTarget;
			} else if (typeof vTarget === "string") {
				this._sOrder = vTarget;
			}
			return this.extractActivities().then(function() {
				return this.prepareOrders();
			}.bind(this)).then(function() {
				this.setLoadingProgress(10);
				return this.extractShopFloorItems();
			}.bind(this)).then(function() {
				this.setLoadingProgress(20);
				return this.extractComponents();
			}.bind(this)).then(function() {
				return this.generateTemporaryShopFloorItems();
			}.bind(this)).then(function() {
				this.setLoadingProgress(30);
				return this.extractInspections();
			}.bind(this)).then(function() {
				this.setLoadingProgress(45);
				return this.extractInspectionValueHelps();
			}.bind(this)).then(function() {
				this.setLoadingProgress(60);
				return this.extractPRTs();
			}.bind(this)).then(function() {
				this.setLoadingProgress(70);
				return Promise.all([
					this.extractDocumentsMetadata(),
					this.extractAttachmentsMetadata()
				]);
			}.bind(this)).then(function() {
				this.setLoadingProgress(80);
				return this.extractAllFiles();
			}.bind(this)).then(function() {
				this.setLoadingProgress(95);
				if (bTransferToIndexedDB) {
					return this.transferAllDataToIndexedDB();
				}
			}.bind(this)).then(function() {
				this.setLoadingProgress(100);
				this.setLoadingState("Success");
				if (this._loadingDialog) {
					this._loadingDialog.close();
				}
			}.bind(this)).catch(function(oError) {
				if (this._loadingDialog) {
					this._loadingDialog.close();
				}
				return Promise.reject(oError);
			}.bind(this));
		},
		
		transferAllDataToIndexedDB: function() {
			var oObjectStoreTransferMap = {
				"C_OperationActivityWorklistTP": this.aActivities,
				"C_ShopFloorItemAtOpActy": this.aShopFloorItems,
				"C_OperationActivityComponent": this.aComponents,
				"C_SFIPlanInspCharInfo": this.aInspections,
				"C_Chargroupcode_Valuehelp": this.aInspectionValueHelps,
				"C_OperationActyPRTAssignment": this.aPRTs,
				"documents": this.aDocuments,
				"attachments": this.aAttachments
			};
			var aIDBRequests = [];
			for (var sObjectStore in oObjectStoreTransferMap) {
				var aObjects = oObjectStoreTransferMap[sObjectStore];
				aObjects.forEach(function(oObject) {
					aIDBRequests.push(this._oIDB.put(sObjectStore, oObject));
				}.bind(this));
			}
			return Promise.all(aIDBRequests);
		},
		
		buildMultiFilterForActivities: function(aActivityKeyMaps) {
			if (!aActivityKeyMaps) {
				aActivityKeyMaps = this._aActivityKeyMaps;
			}
			var aFilters = [];
			aActivityKeyMaps.forEach(function(oActivityKeyMap) {
				aFilters.push(new Filter({
					filters: [
						new Filter("OpActyNtwkElement", "EQ", oActivityKeyMap.OpActyNtwkElement),
						new Filter("OpActyNtwkInstance", "EQ", oActivityKeyMap.OpActyNtwkInstance)
					],
					and: true
				}));
			});
			var oFilter = new Filter({
				filters: aFilters,
				and: false
			});
			return oFilter;
		},
		
		generateTemporaryShopFloorItems: function() {
			function fnKeysMatch(oActivity, oShopFloorItem) {
				return oActivity.OpActyNtwkElement === oShopFloorItem.OpActyNtwkElement
				&& oActivity.OpActyNtwkInstance === oShopFloorItem.OpActyNtwkInstance;
			}
			this.aShopFloorItems.forEach(function(oShopFloorItem) {
				this.aActivities.forEach(function(oActivity) {
					if (!fnKeysMatch(oActivity, oShopFloorItem)) {
						var oNewShopFloorItem = Object.assign({}, oActivity);
						oNewShopFloorItem.ShopFloorItem = oShopFloorItem.ShopFloorItem;
						oNewShopFloorItem.SerialNumber = oShopFloorItem.SerialNumber;
						oNewShopFloorItem.ManufacturingOrderItem = oShopFloorItem.ManufacturingOrderItem;
						oNewShopFloorItem.isTemporary = true;
						oNewShopFloorItem.syncStatus = "No Changes";
						this.aShopFloorItems.push(oNewShopFloorItem);
					}
				}.bind(this));
			}.bind(this));
		},
		
		
		
		prepareOrders: function(aOrders) {
			if (!aOrders) {
				aOrders = [];
				this.aActivities.forEach(function(oActivity) {
					if (aOrders.indexOf(oActivity.ManufacturingOrder) === -1) {
						aOrders.push(oActivity.ManufacturingOrder);
					}
				});
			}
			var sOrder = aOrders.splice(0, 1)[0];
			// var sPath = this._http.createKey("offline", "/I_Order", {OrderID: sOrder}, true);
			return this._http.post("offline", "/MfgOrderSet", {OrderNumber: sOrder}).then(function() {
				if (aOrders.length > 0) {
					return this.prepareOrders(aOrders);
				}
			}.bind(this));
		},
		
		extractActivities: function() {
			var oFilter = new Filter({
				filters: [new Filter("ManufacturingOrder", "EQ", this._sOrder)]
			});
			if (this._sOrder === "") {
				oFilter = this.buildMultiFilterForActivities();
			}
			return this._http.get("activities", "/C_OperationActivityWorklistTP", { filters: [oFilter] }).then(function(aActivities) {
				this.aActivities = aActivities;
				if (this._sOrder) {
					this._aActivityKeyMaps = aActivities.map(function(oActivity) {
						return {
							OpActyNtwkElement: oActivity.OpActyNtwkElement,
							OpActyNtwkInstance: oActivity.OpActyNtwkInstance
						};
					});
				}
				return aActivities;
			}.bind(this));
		},
		
		extractShopFloorItems: function() {
			var oFilter = new Filter({
				filters: [new Filter("ManufacturingOrder", "EQ", this._sOrder)]
			});
			if (this._sOrder === "") {
				oFilter = this.buildMultiFilterForActivities();
			}
			return this._http.get("execution", "/C_ShopFloorItemAtOpActy", { filters: [oFilter] }).then(function(aShopFloorItems) {
				aShopFloorItems.forEach(function(oShopFloorItem) {
					oShopFloorItem.syncStatus = "No Changes";
				});
				this.aShopFloorItems = aShopFloorItems;
				return aShopFloorItems;
			}.bind(this));
		},
		
		extractComponents: function() {
			var oFilter = this.buildMultiFilterForActivities();
			this._http.get("activities", "/C_OperationActivityComponent", {filters: [oFilter] }).then(function(aComponents) {
				this.aComponents = aComponents;
				return aComponents;
			}.bind(this));
		},
		
		extractInspections: function() {
			var oFilter = this.buildMultiFilterForActivities();
			return this._http.get("inspections", "/C_SFIPlanInspCharInfo", { filters: [oFilter] }).then(function(aInspections) {
				this.aInspections = aInspections;
				return aInspections;
			}.bind(this));
		},
		
		extractInspectionValueHelps: function() {
			if (this.aInspections.length === 0) {
				return Promise.resolve([]);
			}
			var aFilters = [];
			this.aInspections.forEach(function(oInspection) {
				aFilters.push(new Filter({
					filters: [
						new Filter("InspectionLot", "EQ", oInspection.InspectionLot),
						new Filter("InspPlanOperationInternalID", "EQ", oInspection.InspPlanOperationInternalID),
						new Filter("InspectionCharacteristic", "EQ", oInspection.InspectionCharacteristic)
					],
					and: true
				}));
			});
			var oFilter = new Filter({
				filters: aFilters,
				and: false
			});
			return this._http.get("inspections", "/C_Chargroupcode_Valuehelp", { filters: [oFilter] }).then(function(aValueHelps) {
				this.aInspectionValueHelps = aValueHelps;
				return aValueHelps;
			}.bind(this));
		},
		
		extractPRTs: function() {
			var oFilter = this.buildMultiFilterForActivities();
			return this._http.get("execution", "/C_OperationActyPRTAssignment", { filters: [oFilter] }).then(function(aPRTs) {
				this.aPRTs = aPRTs;
				return aPRTs;
			}.bind(this));
		},
		
		extractDocumentsMetadata: function() {
			var aDocumentsMetadata = [];
			var aPRTDocs = this.aPRTs.filter(function(oPRT) {
				return oPRT.ProdnRsceToolCategory === "D";
			});
			return Promise.all(
				aPRTDocs.map(function(oPRTDoc) {
					var oUrlParameters = {
						Documentnumber: oPRTDoc.DocumentInfoRecord.padStart(25, "0"),
					    Documentpart: oPRTDoc.DocumentPart,
					    Documenttype: oPRTDoc.DocumentType,
					    Documentversion: oPRTDoc.DocumentVersion,
					    ObjectKey: "",
					    ObjectType: "PORDER"
					};
					return this._http.callFunction("documents", "/GetAllOriginals", oUrlParameters).then(function(oResponse) {
						oResponse.results.forEach(function(oMetadata) {
							aDocumentsMetadata.push(oMetadata);
						});
					});
				}.bind(this))
			).then(function() {
				this.aDocuments = aDocumentsMetadata;
				return aDocumentsMetadata;
			}.bind(this));
		},
		
		extractAttachmentsMetadata: function(aShopFloorItems) {
			if (!aShopFloorItems) {
				aShopFloorItems = this.aShopFloorItems;
			}
			var aAttachmentsMetadata = [];
			return Promise.all(
				aShopFloorItems.map(function(oShopFloorItem) {
					var oUrlParameters = this.buildAttachmentsUrlParameters(oShopFloorItem);
					return this._http.callFunction("attachments", "/GetAllOriginals", oUrlParameters).then(function(oResponse) {
						oResponse.results.forEach(function(oMetadata) {
							oMetadata.ShopFloorItem = oShopFloorItem.ShopFloorItem;
							oMetadata.OpActyNtwkElement = oShopFloorItem.OpActyNtwkElement;
							oMetadata.OpActyNtwkInstance = oShopFloorItem.OpActyNtwkInstance;
							aAttachmentsMetadata.push(oMetadata);
						});
					});
				}.bind(this))
			).then(function() {
				this.aAttachments = aAttachmentsMetadata;
				return aAttachmentsMetadata;
			}.bind(this));
		},
		
		extractAllFiles: function() {
			return Promise.all(
				this.aDocuments.concat(this.aAttachments).map(function(oMetadata) {
					var sKey = this._http.createKey("attachments", "/OriginalContentSet", oMetadata, true);
					var sPath = "/sap/opu/odata/sap/CV_ATTACHMENT_SRV" + sKey + "/$value";
					return fetch(sPath).then(function(oResponse) {
						return oResponse.arrayBuffer();
					}).then(function(oArrayBuffer) {
						oMetadata.arrayBuffer = oArrayBuffer;
					});
				}.bind(this))
			);
		},
		
		buildAttachmentsUrlParameters: function(oShopFloorItem) {
			var sObjectKey = jQuery.sap.padLeft(oShopFloorItem.ManufacturingOrder, 0, 12) +
				"S" + // originally sMfgOrderLinkedObjType
				jQuery.sap.padLeft(oShopFloorItem.ManufacturingOrderItem, 0, 4) + 
				jQuery.sap.padLeft("", " ", 6) + 
				jQuery.sap.padLeft(oShopFloorItem.ManufacturingOrderOperation, 0, 4) +
				jQuery.sap.padLeft(oShopFloorItem.OrderOperationInternalID, " ", 8) +
				jQuery.sap.padLeft("", " ", 4) + 
				oShopFloorItem.OpActyNtwkSegmentType;
			if (/^[0-9]+$/.test(oShopFloorItem.OpActyNtwkElementExternalID)) {
				sObjectKey = sObjectKey + jQuery.sap.padLeft(oShopFloorItem.OpActyNtwkElementExternalID, 0, 10);
			} else {
				sObjectKey = sObjectKey + jQuery.sap.padRight(oShopFloorItem.OpActyNtwkElementExternalID, " ", 10);
			}
			if (oShopFloorItem.SerialNumber) {
				sObjectKey = sObjectKey + jQuery.sap.padLeft(oShopFloorItem.SerialNumber, 0, 18);
			}
			return {
				ObjectType: "PORDER",
				ObjectKey: sObjectKey,
				SemanticObjectType: "",
				IsDraft: false
			};
		}
		
	});
});