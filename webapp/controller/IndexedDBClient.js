sap.ui.define([
	"sap/ui/base/Object",
	"bowdark/performOfflineActivity/controller/IndexedDBConfig"
], function(SAPObject, Config) {
	"use strict";
	/* eslint-disable no-console,no-reserved-keys */
	
	return SAPObject.extend("bowdark.performOfflineActivity.controller.IndexedDBController", {
		
		constructor: function() {
			this.sDBName = Config.dbName;
			this.iDBVersion = Config.dbVersion;
			this.aStores = Config.stores;
			this.onDBReady = new Promise(function(resolve, reject) {
				this.readyDB = resolve;
				this.failDB = reject;
			}.bind(this));
			this.openDB();
		},
		
		mapKeysObjectToArray: function(sEntitySet, oKeys) {
			var vKey = this.aStores.filter(function(oStore) {
				return oStore.entitySet === sEntitySet;
			})[0].keyPath.map(function(sKey) {
				return oKeys[sKey];
			});
			if (vKey.length === 1) {
				vKey = vKey[0];
			}
			return vKey;
		},
		
		arrayBufferToBlob: function(buffer, type) {
			return new Blob([buffer], {type: type});
		},
		
		blobToArrayBuffer: function(blob) {
			return new Promise(function(resolve, reject) {
				var reader = new FileReader();
				reader.addEventListener("loadend", function() {
				  resolve(reader.result);
				});
				reader.addEventListener("error", reject);
				reader.readAsArrayBuffer(blob);
			});
		},
		
		openDB: function() {
			var oRequest = window.indexedDB.open(this.sDBName, this.iDBVersion);
			oRequest.onupgradeneeded = this.onUpgradeNeeded.bind(this);
			oRequest.onerror = function(oError) {
				this.failDB(oError);
			}.bind(this);
			oRequest.onsuccess = function(oEvent) {
				this.readyDB(oEvent.target.result);
			}.bind(this);
		},
		
		onUpgradeNeeded: function(oEvent) {
			var oDB = oEvent.target.result;
			var oExistingStoreNames = oDB.objectStoreNames;
			this.aStores.forEach(function(oStore) {
				if (!oExistingStoreNames.contains(oStore.entitySet)) {
					var vKeyPath = oStore.keyPath.length === 1 ? oStore.keyPath[0] : oStore.keyPath;
					var oNewStore = oDB.createObjectStore(oStore.entitySet, { keyPath: vKeyPath });
					oStore.indexes.forEach(function(oIndex) {
						oNewStore.createIndex(oIndex.property, oIndex.property, { unique: oIndex.unique });
					});
				}
			});
		},
		
		checkDB: function() {
			this.onDBReady.then(function(oDB) {
				console.log("indexed db ready", oDB);
			}).catch(function(oError) {
				console.log("indexed db failed", oError);
			});
		},
		
		clearStore: function(sStore) {
			return this.onDBReady.then(function(oDB) {
				var oDBStore = oDB.transaction([sStore], "readwrite").objectStore(sStore);
				var oClearRequest = oDBStore.clear();
				return new Promise(function(resolve, reject) {
					oClearRequest.onsuccess = resolve;
					oClearRequest.onerror = reject;
				});
			});
		},
		
		clearAllStores: function() {
			return this.onDBReady.then(function(oDB) {
				var oExistingStoreNames = oDB.objectStoreNames;
				var aExistingStoreNames = [];
				for (var i = 0; i < oExistingStoreNames.length; i++) {
					aExistingStoreNames.push(oExistingStoreNames.item(i));
				}
				return Promise.all(
					aExistingStoreNames.map(this.clearStore.bind(this))
				);
			}.bind(this));
		},
		
		get: function(sEntitySet, oKeyMap) {
			return this.onDBReady.then(function(oDB) {
				return new Promise(function(resolve, reject) {
					var oDBStore = oDB.transaction([sEntitySet], "readonly").objectStore(sEntitySet);
					var vKey = this.mapKeysObjectToArray(sEntitySet, oKeyMap);
					var oGetRequest = oDBStore.get(vKey);
					oGetRequest.onsuccess = function() {
						resolve(oGetRequest.result);
					};
					oGetRequest.onerror = function(oError) {
						reject(oError);
					};
				}.bind(this));
			}.bind(this));
		},
		
		getAll: function(sEntitySet) {
			return this.onDBReady.then(function(oDB) {
				return new Promise(function(resolve, reject) {
					var oDBStore = oDB.transaction([sEntitySet], "readonly").objectStore(sEntitySet);
					var oGetAllRequest = oDBStore.getAll();
					oGetAllRequest.onsuccess = function(oEvent) {
						resolve(oEvent.target.result);
					};
					oGetAllRequest.onerror = function(oError) {
						reject(oError);
					};
				});
			});
		},
		
		put: function(sEntitySet, oEntity) {
			return this.onDBReady.then(function(oDB) {
				return new Promise(function(resolve, reject) {
					var oDBStore = oDB.transaction([sEntitySet], "readwrite").objectStore(sEntitySet);
					var oPutRequest = oDBStore.put(oEntity);
					oPutRequest.onsuccess = function() {
						resolve();
					};
					oPutRequest.onerror = function(oError) {
						reject(oError);
					};
				});
			});
		},
		
		delete: function(sEntitySet, oKeyMap) {
			return this.onDBReady.then(function(oDB) {
				return new Promise(function(resolve, reject) {
					var oDBStore = oDB.transaction([sEntitySet], "readwrite").objectStore(sEntitySet);
					var vKey = this.mapKeysObjectToArray(sEntitySet, oKeyMap);
					var oDeleteRequest = oDBStore.delete(vKey);
					oDeleteRequest.onsuccess = resolve;
					oDeleteRequest.onerror = reject;
				}.bind(this));
			}.bind(this));
		}
	});
});