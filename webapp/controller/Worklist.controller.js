sap.ui.define([
	"bowdark/performOfflineActivity/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/Sorter"
], function (BaseController, Filter, Sorter) {
	"use strict";
	/* global bowdark */

	return BaseController.extend("bowdark.performOfflineActivity.controller.Worklist", {
		
		onInit: function() {
			bowdark.performOfflineActivity.controller.BaseController.prototype.onInit.apply(this, arguments);
		},
		
		onAfterRendering: function() {
			this.loadRequiredDataFromIDB();
			this.byId("sfiTable").getBinding("items").sort([
				new Sorter("ManufacturingOrderOperation"),
				new Sorter("OperationActivity")
			]);
		},
		
		onPressActivityListItem: function(oEvent) {
			var sShopFloorItemPath = oEvent.getParameter("listItem").getBindingContext("json").getPath().replace(/\//g, "_");
			this.getOwnerComponent().getRouter().navTo("execution", { path: sShopFloorItemPath });
		},
		
		getSelectedSFIs: function() {
			return this.byId("sfiTable").getSelectedItems().map(function(oItem) {
				return Object.assign({}, oItem.getBindingContext("json").getObject());
			});
		},
		
		onBasicSearch: function() {
			var sValue = this.byId("basicSearchField").getValue().trim();
			var aFilterFields = ["OperationActivityName", "OpActyNtwkElementExternalID", "Material", "SerialNumber", "MfgOrderOperationText", "ManufacturingOrder"];
			var aFilters = aFilterFields.map(function(sFilterField) {
				return new Filter(sFilterField, "Contains", sValue);
			});
			var oFilter = new Filter({
				filters: aFilters,
				and: false
			});
			this.byId("sfiTable").getBinding("items").filter([oFilter]);
		},
		
		onPressResyncSelectedActivities: function() {
			var aSFIs = this.byId("sfiTable").getSelectedItems().map(function(oItem) {
				return oItem.getBindingContext("json").getObject();
			});
			this.resyncMultipleSFIs(aSFIs).then(function(aResyncResults) {
				this.getModel("json").setProperty("/resyncResults", aResyncResults);
				this.openResyncResultsDialog();
			}.bind(this));
		},
		
		formatActivityStatus: function(sStatus) {
			if (sStatus === "SAP_COMPLETED") {
				return "Complete";
			} else if (sStatus === "SAP_IN_PROCESS" ) {
				return "In Process";
			} else if (sStatus === "SAP_INITIAL" || sStatus === "SAP_IN_QUEUE") {
				return "Not Started";
			}
		},
		
		formatActivityState: function(sStatus) {
			if (sStatus === "SAP_COMPLETED") {
				return "Success";
			} else if (sStatus === "SAP_IN_PROCESS") {
				return "Warning";
			} else if (sStatus === "SAP_INITIAL" || sStatus === "SAP_IN_QUEUE") {
				return "Information";
			}
		},
		
		formatSyncState: function(sSyncStatus) {
			if (sSyncStatus === "No Changes") {
				return "Information";
			} else if (sSyncStatus === "Pending Changes") {
				return "Warning";
			} else if (sSyncStatus === "Imported") {
				return "Success";
			}
		}
		
	});
});