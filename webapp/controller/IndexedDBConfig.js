sap.ui.define([], function() {
	"use strict";
	
	return {
		"dbName": "peo",
		"dbVersion": 2,
		"stores": [{
			"entitySet": "C_OperationActivityWorklistTP",
			"service": "MPE_PERSONAL_WORK_QUEUE_SRV",
			"keyPath": ["OpActyNtwkElement", "OpActyNtwkInstance"],
			"indexes": []
		}, {
			"entitySet": "C_SFIPlanInspCharInfo",
			"service": "MPE_EXEC_RR_SRV",
			"keyPath": ["InspectionLot", "InspPlanOperationInternalID", "InspectionSubsetInternalID", "InspectionCharacteristic", "ShopFloorItem"],
			"indexes": [{
				"property": "OpActyNtwkElement",
				"unique": false
			}, {
				"property": "OpActyNtwkInstance",
				"unique": false
			}]
		}, {
			"entitySet": "C_Chargroupcode_Valuehelp",
			"service": "MPE_EXEC_RR_SRV",
			"keyPath": ["SelectedCodeSetPlant", "SelectedCodeSet", "CharacteristicAttributeCode", "InspectionLot", "InspPlanOperationInternalID",
				"InspectionCharacteristic"
			],
			"indexes": [{
				"property": "OpActyNtwkElement",
				"unique": false
			}, {
				"property": "OpActyNtwkInstance",
				"unique": false
			}]
		}, {
			"entitySet": "C_ShopFloorItemAtOpActy",
			"service": "MPE_SFI_EXECUTION_SRV",
			"keyPath":  ["ShopFloorItem", "OpActyNtwkElement", "OpActyNtwkInstance"],
			"indexes": [{
				"property": "OpActyNtwkElement",
				"unique": false
			}, {
				"property": "OpActyNtwkInstance",
				"unique": false
			}, {
				"property": "SerialNumber",
				"unique": false
			}]
		}, {
			"entitySet": "C_OperationActivityComponent",
			"service": "MPE_SFI_EXECUTION_SRV",
			"keyPath":  ["OpActyNtwkElement", "OpActyNtwkInstance", "Reservation", "ReservationItem", "RecordType", "Material"],
			"indexes": [{
				"property": "OpActyNtwkElement",
				"unique": false
			}, {
				"property": "OpActyNtwkInstance",
				"unique": false
			}, {
				"property": "ShopFloorItem",
				"unique": false
			}]
		}, {
			"entitySet": "C_OperationActyPRTAssignment",
			"service": "MPE_SFI_EXECUTION_SRV",
			"keyPath":  ["OpActyNtwkElement", "OpActyNtwkInstance", "OrderInternalID",  "MfgOrderOpProdnRsceToolIntID"],
			"indexes": [{
				"property": "OpActyNtwkElement",
				"unique": false
			}, {
				"property": "OpActyNtwkInstance",
				"unique": false
			}]
		}, {
			"entitySet": "labor",
			"service": "local",
			"keyPath":  ["OpActyNtwkElement", "OpActyNtwkInstance", "ShopFloorItem","Type"],
			"indexes": []
		}, {
			"entitySet": "documents",
			"service": "MPE_DOCUMENT_LIST_SRV",
			"keyPath":  ["Documenttype", "Documentnumber", "Documentpart", "Documentversion"],
			"indexes": []
		}, {
			"entitySet": "attachments",
			"service": "CV_ATTACHMENT_SRV",
			"keyPath": ["Documenttype", "Documentnumber", "Documentpart", "Documentversion", "FileId"],
			"indexes": []
		}]
	};
});