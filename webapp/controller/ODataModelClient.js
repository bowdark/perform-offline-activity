sap.ui.define([
	"sap/ui/base/Object"
], function(SAPObject) {
	"use strict"; 
	/* eslint-disable no-reserved-keys  */
	
	return SAPObject.extend("bowdark.performOfflineActivity.controller.ODataModelClient", {
		
		_oComponent: {},
		_oModels: {},
		_bCreatedWithComponent: true,
		
		constructor: function(vComponentOrModels) {
			if (!Array.isArray(vComponentOrModels)) {
				this.oComponent = vComponentOrModels;
			} else {
				this._bCreatedWithComponent = false;
				vComponentOrModels.forEach(function(oModel) {
					this._oModels[oModel.name] = oModel.model;
				}.bind(this));
			}
		},
		
		_getModel: function(sModel) {
			if (sModel) {
				return this._bCreatedWithComponent ? this.oComponent.getModel(sModel) : this._oModels[sModel];
			} else {
				return this.oComponent.getModel();
			}
		},
		
		setUseCustomHttpRequest: function(bUse) {
			if (typeof bUse !== "boolean") {
				return;
			}
			if (bUse && window.OData.defaultHttpClient.isCustom !== true) {
				var fnDefaultRequest = window.OData.defaultHttpClient.request;
				window.OData.defaultHttpClient.request = function() {
				    if (arguments[0].headers["Content-Encoding"] === "base64") {
				        arguments[0].body = arguments[0].data;
				    }
				    fnDefaultRequest.apply(this, arguments);
				};
				window.OData.defaultHttpClient.isCustom = true;
				window.OData.defaultHttpClient.defaultRequest = fnDefaultRequest;
			} else if (bUse === false && window.OData.defaultHttpClient.isCustom === true) {
				window.OData.defaultHttpClient.request = window.OData.defaultHttpClient.defaultRequest;
				window.OData.defaultHttpClient.isCustom = false;
			}
		},
		
		createKey: function(sModel, sPath, oKeyMap, bSynchronous) {
			if (bSynchronous === true) {
				return this._getModel(sModel).createKey(sPath, oKeyMap);
			}
			return this._getModel(sModel).metadataLoaded().then(function() {
				return this._getModel(sModel).createKey(sPath, oKeyMap);
			}.bind(this));
		},
		
		post: function(sModel, sPath, oPayload, oParameters) {
			if (!oParameters) {
				oParameters = {};
			}
			if (oParameters.headers) {
				this._getModel(sModel).setHeaders(oParameters.headers);
				delete oParameters.headers;
			}
			return this._getModel(sModel).metadataLoaded().then(function() {
				return new Promise(function(resolve, reject) {
					oParameters = {
						success: function(oResponse) {
							this._getModel(sModel).setHeaders();
							resolve(oResponse);
						}.bind(this),
						error: function(oError) {
							this._getModel(sModel).setHeaders();
							reject(oError);
						}.bind(this)
					};
					this._getModel(sModel).create(sPath, oPayload, oParameters);
				}.bind(this));
			}.bind(this));
		},
		
		get: function(sModel, sPath, oParams) {
			if (!oParams) {
				oParams = {};
			}
			return this._getModel(sModel).metadataLoaded().then(function() {
				return new Promise(function(resolve, reject) {
					oParams.success = function(oResponse) {
						if (Array.isArray(oResponse.results)) {
							resolve(oResponse.results);
						} else {
							resolve(oResponse);
						}
					};
					oParams.error = function(oError) {
						reject(oError);
					};
					this._getModel(sModel).read(sPath, oParams);
				}.bind(this));
			}.bind(this));
		},
		
		update: function(sModel, sPath, oPayload) {
			for (var sProperty in oPayload) {
				this._getModel(sModel).setProperty(sPath + "/" + sProperty, oPayload[sProperty]);
			}
			return this._getModel(sModel).metadataLoaded().then(function() {
				return new Promise(function(resolve, reject) {
					this._getModel(sModel).update(sPath, oPayload, {
						success: function() {
							resolve();
						},
						error: function(oError) {
							reject(oError);
						}
					});
				}.bind(this));
			}.bind(this));
		},
		
		delete: function(sModel, sPath, sGroupId) {
			return this._getModel(sModel).metadataLoaded().then(function() {
				return new Promise(function(resolve, reject) {
					var oParameters = {
						success: function(oResponse) {
							resolve(oResponse);
						},
						error: function(oError) {
							reject(oError);
						}
					};
					if (sGroupId) {
						oParameters.groupId = sGroupId;
					}
					this._getModel(sModel).remove(sPath, oParameters);
				}.bind(this));
			}.bind(this));
		},
		
		callFunction: function(sModel, sPath, oURLParameters, oParameters) {
			if (!oParameters) {
				oParameters = {};
			}
			return this._getModel(sModel).metadataLoaded().then(function() {
				return new Promise(function(resolve, reject) {
					oParameters.success = resolve;
					oParameters.error = reject;
					oParameters.urlParameters = oURLParameters;
					this._getModel(sModel).callFunction(sPath, oParameters);
				}.bind(this));
			}.bind(this));
		},
		
		parseJSONError: function (oResponse) {
			try {
				var json = JSON.parse(oResponse.responseText);
				var oError = json.error;
				var sMessage = "Server Error: " + oError.message.value;
				if (oError.innererror && oError.innererror.errordetails && oError.innererror.errordetails.length > 0) {
					var aMessages = oError.innererror.errordetails.filter(function (e) {
						return e.code.indexOf("/IWBEP") === -1 && (e.code !== "" || e.message !== "");
					}).map(function (e) {
						if (e.code.length > 0) {
							return "Error Code " + e.code + ": " + e.message;
						} else {
							return "Server Error: " + e.message;
						}
					});
					aMessages = this.removeDuplicatesFromArray(aMessages);
					var sMsg = aMessages.join("\n");
					if (sMsg && sMsg.trim().length > 1) {
						sMessage = sMsg;
					}
				}
				return sMessage;
			} catch (e) {
				return undefined;
			}
		},
		
		removeDuplicatesFromArray: function (aArray) {
			return aArray.filter(function (v, i) {
				return aArray.indexOf(v) === i;
			});
		},
		
		arrayBufferToBinaryString: function(oArrayBuffer) {
			return String.fromCharCode.apply(null, Array.prototype.slice.apply(new Uint8Array(oArrayBuffer)));
		}
		
	});
});