sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment",
	"sap/base/util/deepEqual",
	"bowdark/performOfflineActivity/controller/IndexedDBClient",
	"bowdark/performOfflineActivity/controller/ODataModelClient",
	"bowdark/performOfflineActivity/controller/OperationActivityDataExtractor"
], function (Controller, Filter, MessageBox, MessageToast, Fragment, deepEqual, IndexedDBClient, ODataModelClient, OAExtractor) {
	"use strict";
	/* eslint-disable no-console */

	return Controller.extend("bowdark.performOfflineActivity.controller.BaseController", {
		
		_oIDBClient: new IndexedDBClient(),
		
		http: null,
		
		onInit: function() {
			this.http = new ODataModelClient(this);
		},
		
		extractTestData: function(sOrderNumber, bTransferToIDB, bShowLoadingDialog) {
			var oExtractor = new OAExtractor(this.getOwnerComponent());
			return oExtractor.extract(sOrderNumber, bTransferToIDB, bShowLoadingDialog).then(function() {
				MessageToast.show("Succesfully Exported Data");
			}).catch(console.log);
		},
		
		getModel: function(sName) {
			return this.getView().getModel(sName);
		},
		
		loadRequiredDataFromIDB: function () {
			if (this.getModel("json").getProperty("/allDataLoaded")) {
				return Promise.resolve();
			}
			this.getView().setBusy(true);
			this.getModel("json").setProperty("/allDataLoaded", false);
			return Promise.all([
					this.getActivities(),
					this.getShopFloorItems(),
					this.getComponents(),
					this.getInspectionCharacteristics(),
					this.getInspValueHelp(),
					this.getPRTs()
			]).then(function() {
				this.getModel("json").setProperty("/allDataLoaded", true);
				this.getView().setBusy(false);
			}.bind(this)).catch(function(oError) {
				this.getView().setBusy(false);
				MessageBox.error("Failed to load all required data");
				console.log(oError);
			}.bind(this));
		},
		
		getActivities: function() {
			return this._oIDBClient.getAll("C_OperationActivityWorklistTP").then(function(aResults) {
				this.getModel("json").setProperty("/activities", aResults);
			}.bind(this));
		},
		
		getShopFloorItems: function() {
			return this._oIDBClient.getAll("C_ShopFloorItemAtOpActy").then(function(aResults) {
				this.getModel("json").setProperty("/shopFloorItems", aResults);
			}.bind(this));
		},
		
		getComponents: function() {
			return this._oIDBClient.getAll("C_OperationActivityComponent").then(function(aResults) {
				this.getModel("json").setProperty("/components", aResults);
			}.bind(this));
		},
		
		getInspectionCharacteristics: function() {
			return this._oIDBClient.getAll("C_SFIPlanInspCharInfo").then(function(aResults) {
				this.getModel("json").setProperty("/inspections", aResults);
			}.bind(this));
		},
		
		getInspValueHelp: function() {
			return this._oIDBClient.getAll("C_Chargroupcode_Valuehelp").then(function(aResults) {
				this.getModel("json").setProperty("/inspectionVH", aResults);
			}.bind(this));
		},
		
		getPRTs: function() {
			return this._oIDBClient.getAll("C_OperationActyPRTAssignment").then(function(aResults) {
				this.getModel("json").setProperty("/prts", aResults);
			}.bind(this));
		},
		
		setLoadingProgress: function(iPercentage) {
			try {
				this._loadingDialog.getContent()[0].getItems()[1].setPercentValue(iPercentage);
			} catch (oError) {
				//
			}
		},
		
		getLoadingProgress: function() {
			try {
				return this._loadingDialog.getContent()[0].getItems()[1].getPercentValue();
			} catch (oError) {
				//
			}
		},
		
		setLoadingState: function(sState) {
			try {
				this._loadingDialog.getContent()[0].getItems()[1].setState(sState);
			} catch (oError) {
				//
			}
		},
		
		increaseLoadingProgress: function(iValue) {
			var iCurrent = this.getLoadingProgress();
			var iProgress = iCurrent + iValue;
			this.setLoadingProgress(iProgress);
		},
		
		_openLoadingDialog: function() {
			if (!this._loadingDialog) {
				this._loadingDialog = new sap.m.Dialog({
					showHeader: false,
					contentWidth: "500px",
					afterClose: function() {
						this._loadingDialog.destroy();
						delete this._loadingDialog;
					}.bind(this),
					content: [
						new sap.m.VBox({
							justifyContent: "Center",
							items: [
								new sap.m.Text({text: "Importing Data..."}),
								new sap.m.ProgressIndicator({
									width: "400px",
									state: "Information",
									percentValue: 0,
									showValue: false
								})
							]
						}).addStyleClass("sapUiMediumMargin")
					]
				});
				this.setLoadingProgress(0);
				this.setLoadingState("Information");
				this._loadingDialog.open();
			}
		},
		
		resyncMultipleSFIs: function(aShopFloorItems, aResyncResults, iStepSize) {
			if (!iStepSize) {
				iStepSize = Math.round(100 / aShopFloorItems.length);
			}
			var oShopFloorItem = aShopFloorItems.splice(0, 1)[0];
			if (!aResyncResults) {
				aResyncResults = [];
			}
			if (!this._loadingDialog) {
				this._openLoadingDialog();
			}
			return this.resyncSFI(oShopFloorItem).then(function(aResults) {
				this.increaseLoadingProgress(iStepSize);
				aResyncResults = aResyncResults.concat(aResults);
				if (aShopFloorItems.length > 0) {
					return this.resyncMultipleSFIs(aShopFloorItems, aResyncResults, iStepSize);
				} else {
					this._loadingDialog.close();
					return aResyncResults;
				}
			}.bind(this));
		},
		
		_getModelPathForSFI: function(oShopFloorItem) {
			var aSFIs = this.getModel("json").getProperty("/shopFloorItems");
			var sPath;
			for (var i = 0; i < aSFIs.length; i++) {
				var bMatch = oShopFloorItem.ShopFloorItem === aSFIs[i].ShopFloorItem
					&& oShopFloorItem.OpActyNtwkElement === aSFIs[i].OpActyNtwkElement
					&& oShopFloorItem.OpActyNtwkInstance === aSFIs[i].OpActyNtwkInstance;
				if (bMatch) {
					break;
				}
			}
			sPath = "/shopFloorItems/" + i;
			return sPath;
		},
		
		_setSFIModelProperty: function(oShopFloorItem, sProperty, vValue) {
			var sPath = this._getModelPathForSFI(oShopFloorItem);
			this.getModel("json").setProperty(sPath + "/" + sProperty, vValue);
		},
		
		resyncSFI: function(oShopFloorItem) {
			var aResyncResults = [];
			return this.setActivityForRetroactiveProcessing(oShopFloorItem).then(function() {
				return this.resyncAttachments(oShopFloorItem);
			}.bind(this)).then(function(aResults) {
				aResyncResults = aResyncResults.concat(aResults);
				return this.resyncComponents(oShopFloorItem);
			}.bind(this)).then(function(aResults) {
				aResyncResults = aResyncResults.concat(aResults);
				return this.resyncInspections(oShopFloorItem);
			}.bind(this)).then(function(aResults) {
				aResyncResults = aResyncResults.concat(aResults);
				return this.resyncLabor(oShopFloorItem);
			}.bind(this)).then(function(aResults) {
				aResyncResults = aResyncResults.concat(aResults);
			}).then(function() {
				aResyncResults.forEach(function(oResult) {
					oResult.activity = oShopFloorItem.OpActyNtwkElementExternalID + " " + oShopFloorItem.OperationActivityName;
					oResult.order = oShopFloorItem.ManufacturingOrder;
					oResult.operation = oShopFloorItem.ManufacturingOrderOperation;
				});
				oShopFloorItem.resyncStatus = "Imported";
				this._setSFIModelProperty(oShopFloorItem, "syncStatus", "Imported");
				this._oIDBClient.put("C_ShopFloorItemAtOpActy", oShopFloorItem);
				return aResyncResults;
			}.bind(this));
		},
		
		setActivityForRetroactiveProcessing: function(oShopFloorItem) {
			var oUrlParams = {
				OpActyNtwkInstance: oShopFloorItem.OpActyNtwkInstance,
				OpActyNtwkElement: oShopFloorItem.OpActyNtwkElement
			};
			var oActivity;
			return this._oIDBClient.get("C_OperationActivityWorklistTP", oShopFloorItem).then(function(oActy) {
				oActivity = oActy;
				var bRetroIsSetBeSet = oActivity.OpActyIsSeldForRtactvPostg;
				return bRetroIsSetBeSet;
			}).then(function(bRetroIsSet) {
				if (!bRetroIsSet) {
					return this.http.callFunction("activities", "/A0A04AF2065A6A93D120CA4F9B18Start_rtactv", oUrlParams, {method: "POST"});
				}
			}.bind(this)).then(function() {
				oActivity.OpActyIsSeldForRtactvPostg = true;
				return this._oIDBClient.put("C_OperationActivityWorklistTP", oActivity);
			}.bind(this));
		},
		
		buildObjectKeyForAttachments: function(oShopFloorItem) {
			var sObjectKey = jQuery.sap.padLeft(oShopFloorItem.ManufacturingOrder, 0, 12) +
				"S" + // originally sMfgOrderLinkedObjType
				jQuery.sap.padLeft(oShopFloorItem.ManufacturingOrderItem, 0, 4) + 
				jQuery.sap.padLeft("", " ", 6) + 
				jQuery.sap.padLeft(oShopFloorItem.ManufacturingOrderOperation, 0, 4) +
				jQuery.sap.padLeft(oShopFloorItem.OrderOperationInternalID, " ", 8) +
				jQuery.sap.padLeft("", " ", 4) + 
				oShopFloorItem.OpActyNtwkSegmentType;
			if (/^[0-9]+$/.test(oShopFloorItem.OpActyNtwkElementExternalID)) {
				sObjectKey = sObjectKey + jQuery.sap.padLeft(oShopFloorItem.OpActyNtwkElementExternalID, 0, 10);
			} else {
				sObjectKey = sObjectKey + jQuery.sap.padRight(oShopFloorItem.OpActyNtwkElementExternalID, " ", 10);
			}
			sObjectKey = sObjectKey + jQuery.sap.padLeft(oShopFloorItem.SerialNumber, 0, 18);
			return sObjectKey;
		},
		
		refreshAttachmentsFromIDB: function(oShopFloorItem) {
			this._oIDBClient.getAll("attachments").then(function(aResults) {
				var aAttachments = aResults.filter(function(oAttachment) {
					return oAttachment.OpActyNtwkElement === oShopFloorItem.OpActyNtwkElement
						&& oAttachment.OpActyNtwkInstance === oShopFloorItem.OpActyNtwkInstance
						&& oAttachment.ShopFloorItem === oShopFloorItem.ShopFloorItem;
				});
				this.getModel("json").setProperty("/attachments", aAttachments);
			}.bind(this));
		},
		
		refreshAttachmentsFromServer: function(oShopFloorItem) {
			var oExtractor = new OAExtractor(this.getOwnerComponent());
			oExtractor.extractAttachmentsMetadata([oShopFloorItem]).then(function() {
				return oExtractor.extractAllFiles();
			}).then(function() {
				return Promise.all(
					oExtractor.aAttachments.map(function(oAttachment) {
						this._oIDBClient.put("attachments", oAttachment);
					}.bind(this))
				);
			}.bind(this));
		},
		
		_buildAttachmentCreateRequests: function(oShopFloorItem, aAttachments) {
			return aAttachments.map(function(oAttachment) {
				var sObjectKey = this.buildObjectKeyForAttachments(oShopFloorItem);
				return {
					attachment: oAttachment,
					body: new Blob([oAttachment.arrayBuffer], {type: oAttachment.ContentType }),
					objectKey: sObjectKey,
					headers: {
						objectkey: btoa(encodeURIComponent(sObjectKey)),
						objecttype: "PORDER",
						semanticobjecttype: "",
						documentType: "",
						documentNumber: "",
						documentPart: "",
						documentVersion: "",
						"content-type": oAttachment.ContentType,
						"Content-Encoding": "base64",
						slug: btoa(encodeURIComponent(oAttachment.Filename))
					}
				};
			}.bind(this));
		},
		
		getAttachmentsForSFI: function(oShopFloorItem) {
			return this._oIDBClient.getAll("attachments").then(function(aAttachments) {
				return aAttachments.filter(function(oAttachment) {
					return oAttachment.ShopFloorItem === oShopFloorItem.ShopFloorItem
						&& oAttachment.OpActyNtwkElement === oShopFloorItem.OpActyNtwkElement
						&& oAttachment.OpActyNtwkInstance === oShopFloorItem.OpActyNtwkInstance
						&& oAttachment.createdOffline === true;
				});
			});
		},
		
		getInspectionsForSFI: function(oShopFloorItem) {
			return this._oIDBClient.getAll("C_SFIPlanInspCharInfo").then(function(aInspections) {
				return aInspections.filter(function(oInspection) {
					return oInspection.ShopFloorItem === oShopFloorItem.ShopFloorItem
						&& oInspection.OpActyNtwkElement === oShopFloorItem.OpActyNtwkElement
						&& oInspection.OpActyNtwkInstance === oShopFloorItem.OpActyNtwkInstance;
				});
			});
		},
		
		resyncAttachments: function(oShopFloorItem) {
			var aResyncResults = [];
			var aAttachmentsToDeleteFromIDB = [];
			var sObjectKey = this.buildObjectKeyForAttachments(oShopFloorItem);
			var bSkipRefresh = false;
			this.http.setUseCustomHttpRequest(true);
			return this.getAttachmentsForSFI(oShopFloorItem).then(function(aAttachments) {
				var aRequests = this._buildAttachmentCreateRequests(oShopFloorItem, aAttachments);
				if (!aRequests || aRequests.length === 0) {
					bSkipRefresh = true;
					return Promise.resolve();
				}
				return Promise.all(
					aRequests.map(function(oRequest) {
						return this.http.post("attachments", "/OriginalContentSet", oRequest.body, { headers: oRequest.headers })
						.then(function() {
							aResyncResults.push({
								object: "Attachment - " + oRequest.attachment.Filename,
								success: true,
								message: ""
							});
							aAttachmentsToDeleteFromIDB.push(oRequest.attachment);
							return Promise.resolve(true);
						}).catch(function(oError) {
							aResyncResults.push({
								object: "Attachment - " + oRequest.attachment.Filename,
								success: false,
								message: this.http.parseJSONError(oError)
							});
							return Promise.resolve(true);
						}.bind(this));
					}.bind(this)));
			}.bind(this)).then(function() {
				this.http.setUseCustomHttpRequest(false);
				if (bSkipRefresh) {
					return Promise.resolve();
				}
				var oUrlParameters = {
					ObjectKey: sObjectKey,
					ObjectType: "PORDER",
					SemanticObjectType: ""
				};
				return this.http.callFunction("attachments", "/ConfirmAttachment", oUrlParameters, {method: "POST"});
			}.bind(this)).then(function() {
				if (bSkipRefresh) {
					return Promise.resolve();
				}
				return Promise.all(
					aAttachmentsToDeleteFromIDB.map(function(oAttachment) {
						return this._oIDBClient.delete("attachments", oAttachment);
					}.bind(this))
				);
			}.bind(this)).then(function() {
				if (bSkipRefresh) {
					return Promise.resolve();
				}
				return this.refreshAttachmentsFromServer(oShopFloorItem);
			}.bind(this)).then(function() {
				if (bSkipRefresh) {
					return Promise.resolve([]);
				}
				return aResyncResults;
			});
		},
		
		buildUrlParamsForComponents: function(oShopFloorItem, oComponent) {
			return {
				Componentshopflooritem: 0,
				SerialNumber: "",
				Serialnumber: "",
				Batch: "",
				Quantity: oComponent.MaterialComponentQuantity,
				Unitofmeasure: oComponent.BaseUnit,
				OpActyNtwkInstance: oShopFloorItem.OpActyNtwkInstance,
				OpActyNtwkElement: oShopFloorItem.OpActyNtwkElement,
				ShopFloorItem: oShopFloorItem.ShopFloorItem,
				Plant: oComponent.Plant,
				Material: oComponent.Material,
				Reservation: oComponent.Reservation,
				Reservationitem: oComponent.ReservationItem,
				Recordtype: "",
				Storagelocation: oComponent.StorageLocation,
				OrigOanInstanceId: oShopFloorItem.OpActyNtwkInstance,
				OrigOanElementNumber: oShopFloorItem.OpActyNtwkElement,
				StckType: ""
			};
		},
		
		resyncComponents: function(oShopFloorItem) {
			var aResyncResults = [];
			return this._oIDBClient.getAll("C_OperationActivityComponent").then(function(aComponents) {
				return aComponents.filter(function(oComponent) {
					return oComponent.OpActyNtwkElement === oShopFloorItem.OpActyNtwkElement
						&& oComponent.OpActyNtwkInstance === oShopFloorItem.OpActyNtwkInstance;
						// && oComponent.ShopFloorItem === oShopFloorItem.ShopFloorItem
				});
			}).then(function(aComponents) {
				return Promise.all(
					aComponents.map(function(oComponent) {
						if (oComponent.MaterialComponentQuantity == 0) {
							return Promise.resolve();
						}
						var oUrlParams = this.buildUrlParamsForComponents(oShopFloorItem, oComponent);
						return this.http.callFunction("execution", "/C_ShopFloorItemAtOpActyAssemblesficomp", oUrlParams, {method: "POST"}).then(function() {
							aResyncResults.push({
								object: "Component " + oComponent.Material + " - " + oComponent.MaterialComponentQuantity,
								success: true,
								message: ""
							});
							return Promise.resolve();
						}).catch(function(oError) {
							aResyncResults.push({
								object: "Component " + oComponent.Material + " - " + oComponent.MaterialComponentQuantity,
								success: false,
								message: this.http.parseJSONError(oError)
							});
							return Promise.resolve();
						}.bind(this));
					}.bind(this))
				).then(function() {
					return aResyncResults;
				});
			}.bind(this));
		},
		
		postInspections: function(aInspections, aResyncResults) {
			if (!aResyncResults) {
				aResyncResults = [];
			}
			var oInspection = aInspections.splice(0, 1)[0];
			oInspection.to_SFIResultDetails = [];
			return this.http.post("inspections", "/C_SFIPlanInspCharInfo", oInspection).then(function() {
				aResyncResults.push({
					object: "Inspection Characteristic " + oInspection.InspectionSpecificationText + "(" + oInspection.InspectionCharacteristic + ")",
					success: true,
					message: ""
				});
				return Promise.resolve();
			}).catch(function(oError) {
				aResyncResults.push({
					object: "Inspection Characteristic " + oInspection.InspectionSpecificationText + "(" + oInspection.InspectionCharacteristic + ")",
					success: false,
					message: this.http.parseJSONError(oError)
				});
				return Promise.resolve();
			}.bind(this)).then(function() {
				if (aInspections.length > 0) {
					return this.postInspections(aInspections, aResyncResults);
				} else {
					return aResyncResults;
				}
			}.bind(this));
		},
		
		resyncInspections: function(oShopFloorItem) {
			return this.getInspectionsForSFI(oShopFloorItem).then(function(aInspections) {
				if (aInspections.length === 0) {
					return Promise.resolve([]);
				} else {
					return this.postInspections(aInspections);
				}
			}.bind(this));
		},
		
		calculateSecondsLaboredOn: function(oShopFloorItem) {
			return this._oIDBClient.getAll("labor").then(function(aLaborEntries) {
				aLaborEntries = aLaborEntries.filter(function(oLaborEntry) {
					return oLaborEntry.OpActyNtwkElement === oShopFloorItem.OpActyNtwkElement
						&& oLaborEntry.OpActyNtwkInstance === oShopFloorItem.OpActyNtwkInstance
						&& oLaborEntry.ShopFloorItem === oShopFloorItem.ShopFloorItem;
				});
				var oStartEntry = aLaborEntries.find(function(oLaborEntry) {
					return oLaborEntry.Type === "START";
				});
				var oEndEntry = aLaborEntries.find(function(oLaborEntry) {
					return oLaborEntry.Type === "COMPLETE";
				});
				var iTotalTimeInSeconds = (oEndEntry.Time - oStartEntry.Time) / 1000;
				return Math.round(iTotalTimeInSeconds);
			});
		},
		
		resyncLabor: function(oShopFloorItem) {
			var iDurationInSeconds;
			return this.calculateSecondsLaboredOn(oShopFloorItem).then(function(iSeconds) {
				iDurationInSeconds = iSeconds;
				var oUrlParams = {
					Action: "SAP_COMPLETE",
					OpActyNtwkInstance: oShopFloorItem.OpActyNtwkInstance,
					OpActyNtwkElement: oShopFloorItem.OpActyNtwkElement,
					ShopFloorItem: oShopFloorItem.ShopFloorItem,
					Note: "",
					Manufacturingactionreasoncode: "",
					Mfgactionreasoncodegroup: "",
					Mfgactionreasoncodegroupctlg: "",
					Opactyactualdurninseconds: iSeconds,
					Opactyactllabordurninseconds: 0,
					Changealertsacknowledge: false
				};
				return this.http.callFunction("execution", "/C_ShopFloorItemAtOpActyExecutesfisaction", oUrlParams, {method: "POST"});
			}.bind(this)).then(function() {
				return [{
					object: "Time Labored On",
					success: true,
					message: "Time recorded: " + new Date(iDurationInSeconds * 1000).toISOString().substr(11, 8)
				}];
			}).catch(function(oError) {
				return [{
					object: "Time Labored On",
					success: false,
					message: this.http.parseJSONError(oError)
				}];
			}.bind(this));
		},
		
		openResyncResultsDialog: function() {
			Fragment.load({
				id: this.getView().getId(),
				name: "bowdark.performOfflineActivity.fragment.ResyncResultsDialog",
				controller: this
			}).then(function(oDialog) {
				if (this.getModel("device").getProperty("/system/desktop")) {
					oDialog.addStyleClass("sapUiSizeCompact");
				}
				this.getView().addDependent(oDialog);
				oDialog.attachAfterClose(function() {
					oDialog.destroy();
				});
				oDialog.getEndButton().attachPress(function() {
					oDialog.close();
				});
				oDialog.open();
			}.bind(this));
		}

	});
});